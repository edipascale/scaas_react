import React, { Component } from 'react'
import SCaaS from '../build/contracts/SCaaS.json'
import SCaaSInstance from '../build/contracts/SCaaSInstance.json'
import getWeb3 from './utils/getWeb3'
// import update from 'immutability-helper';

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class Balance extends React.Component {

  render() {
    return (
      <div className="Balance">
        <p>The balance of {this.props.name} (@address {this.props.address}) is {this.props.balance} eth </p>
      </div>
    );
  }
}

class Provider extends React.Component {

  render() {
    if (this.props.address != null) {
      return (
        <div className="Provider">
          <p>ECGI: {this.props.ecgi}, cost per Kb: {this.props.costPerKb} eth</p>
          <p>Credit: {this.props.credit} eth, Contract Balance: {this.props.contractBalance} eth</p>
          <Balance address={this.props.address} name={this.props.name} balance={this.props.balance} />
        </div>
      );
    } else {
      return (
        <div className="UndefinedProvider">
          <p>Provider address still undefined, wait for the state to update...</p>
        </div>
      );
    }
  }
}

class UpdateBalanceButton extends React.Component {
  // This syntax ensures `this` is bound within handleClick.
  // Warning: this is *experimental* syntax.
  handleClick = () => {
    this.props.instance.updateBalance(100, {from:this.props.netOp}).then((tx_id) => {
      // console.log(tx_id)
    })
  }

  render() {
    return (
      <button onClick={this.handleClick} className="pure-button pure-button-primary">
        Simulate Traffic (100kb)
      </button>
    );
  }
}

class ClearBalanceButton extends React.Component {
  // This syntax ensures `this` is bound within handleClick.
  // Warning: this is *experimental* syntax.
  handleClick = () => {
    this.props.instance.clearBalance({from:this.props.netOp}).then((tx_id) => {
      // console.log(tx_id)
    })
  }

  render() {
    return (
      <button onClick={this.handleClick} className="pure-button pure-button-primary">
        Reset Demo
      </button>
    );
  }
}

class WithdrawButton extends React.Component {
  // This syntax ensures `this` is bound within handleClick.
  // Warning: this is *experimental* syntax.
  handleClick = () => {
    this.props.instance.withdraw({from:this.props.scOwner}).then((tx_id) => {
      console.log("Withdraw transaction: ", tx_id)
    })
  }

  render() {
    return (
      <button onClick={this.handleClick} className="pure-button pure-button-primary">
        Withdraw credit
      </button>
    );
  }
}

class FundButton extends React.Component {
  // This syntax ensures `this` is bound within handleClick.
  // Warning: this is *experimental* syntax.
  handleClick = () => {
    this.props.instance.sendTransaction({from:this.props.netOp,
                        value:this.props.web3.toWei(0.01, "ether")}).then((tx_id) => {
      // console.log(tx_id)
    })
  }

  render() {
    return (
      <button onClick={this.handleClick} className="pure-button pure-button-primary">
        Increase Funds (0.01 ether)
      </button>
    );
  }
}

class PriceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: 1};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    let value = parseInt(this.state.value, 10)
    if (value > 0) {
      this.props.instance.changeCost(this.props.web3.toWei(value, "gwei"), {from:this.props.netOp}).then((tx_id) => {
        // console.log(tx_id)
      })
    } else {
      console.log("Non-positive cost cannot be accepted")
    }
  }

  render() {
    return (
      <form className="pure-form" onSubmit={this.handleSubmit}>
        <fieldset>
          <label>
            Cost per Kb (in GWei):
            <input type="number" value={this.state.value} onChange={this.handleChange} />
          </label>
          <button type="submit" className="pure-button pure-button-primary">Change Cost</button>
        </fieldset>
      </form>
    );
  }
}

class EventList extends React.Component {

  render() {
    return (
      <ul className="list-group">
        {this.props.events.map(e => <li key={e.value}>{e.value}</li>)}
      </ul>
    )
  }
}

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      web3: null,
      scaasHub: null,
      contractInstance: null,
      contractBalance: 0,
      netOp: null,
      netOpBalance: 0,
      scOwner: null,
      scOwnerBalance: 0,
      ecgi: 2725153600,
      costPerKb: 1000000000, // 1 GWei
      credit: 0,
      events: []
    }
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
    .then(results => {
      this.setState({
        web3: results.web3
      })
      // Instantiate contract once web3 provided.
      this.instantiateContract()
    }).catch((error) => {
      console.error(error)
    })
  }

  instantiateContract() {
    /*
     * SMART CONTRACT EXAMPLE
     *
     * Normally these functions would be called in the context of a
     * state management library, but for convenience I've placed them here.
     */

    const contract = require('truffle-contract')
    const scaas = contract(SCaaS)
    const scaasInstance = contract(SCaaSInstance)
    scaas.setProvider(this.state.web3.currentProvider)
    scaasInstance.setProvider(this.state.web3.currentProvider)

    // Get accounts.
    this.state.web3.eth.getAccounts((error, accounts) => {
      // change this as required for the Rinkeby testnet
      this.setState({netOp: accounts[0], scOwner: accounts[1]})
      scaas.deployed().then((instance) => {
        this.setState({scaasHub: instance})
        // Check whether we have a provider registered for our default ecgi
        return this.state.scaasHub.register.call(this.state.ecgi)
      }).then((result) => {
        console.log("register[ecgi]: ", result)
          if (result === "0x0000000000000000000000000000000000000000") {
            // Register the scOwner as a provider for our default ecgi and unitary costPerKb
            console.log("contract instance not found, creating new one")
            this.state.scaasHub.registerProvider(this.state.ecgi,
                                this.state.scOwner, this.state.costPerKb,
                                {from: this.state.netOp, gas:2000000}).then((tx_id) => {
              for (var i = 0; i < tx_id.logs.length; i++) {
                var log = tx_id.logs[i]
                if (log.event === "ProviderRegistered" &&
                    log.args._provider === this.state.scOwner) {
                  console.log("contract instance created at address: ",  log.args._contract)
                  return this.setState({contractInstance: scaasInstance.at(log.args._contract)})
                  // found = true
                  // break
                }
              }
              // should never get here
              console.error("ProviderRegistered event not found after call to registerProvider()");
            })
          } else {
            // return the already deployed contract for the given ecgi
            console.log("contract instance found at address: ",  result)
            return this.setState({contractInstance: scaasInstance.at(result)})
          }
      }).then(() => {
        // update eth balance of netOp and scOwner
        this.updateEthBalance()
        // there might be pre-existing credit in the contract
        this.updateCredit()
        // check the costPerKb on the contract
        this.updateCost()
        // initialize event listeners
        var _this = this
        var balanceUpdateEvent = this.state.contractInstance.BalanceUpdate()
        var balanceClearedEvent = this.state.contractInstance.DebugBalanceCleared()
        var withdrawalEvent = this.state.contractInstance.Withdrawal()
        var fundIncreaseEvent = this.state.contractInstance.FundIncrease()
        var costChangeEvent = this.state.contractInstance.CostChange()
        var insufficientFundsEvent = this.state.contractInstance.InsufficientFunds()

        balanceUpdateEvent.watch(function(error, result) {
          if (!error) {
            let value = _this.state.web3.fromWei(result.args._valueAdded, "ether").toNumber()
            var timestamp = (new Date()).toUTCString()
            const newEvent = {value: timestamp + ": new BalanceUpdate Event for " + value + " eth"}
            _this.setState({events: _this.state.events.concat(newEvent)})
            _this.updateCredit()
          } else {
            console.error(error)
          }
        })

        balanceClearedEvent.watch(function(error, result) {
          if (!error) {
            var timestamp = (new Date()).toUTCString()
            const newEvent = {value: timestamp + ": new DebugBalanceCleared Event"}
            _this.setState({events: _this.state.events.concat(newEvent)})
            _this.updateCredit()
          } else {
            console.error(error)
          }
        })

        withdrawalEvent.watch(function(error, result) {
          if (!error) {
            var timestamp = (new Date()).toUTCString()
            const newEvent = {value: timestamp + ": new Withdrawal Event"}
            _this.setState({events: _this.state.events.concat(newEvent)})
            _this.updateEthBalance()
            _this.updateCredit()
          } else {
            console.error(error)
          }
        })

        fundIncreaseEvent.watch(function(error, result) {
          if (!error) {
            var timestamp = (new Date()).toUTCString()
            let value = _this.state.web3.fromWei(result.args._value, "ether").toNumber()
            const newEvent = {value: timestamp + ": new FundIncrease Event, value: " + value + " eth" }
            _this.setState({events: _this.state.events.concat(newEvent)})
            _this.updateEthBalance()
          } else {
            console.error(error)
          }
        })

        costChangeEvent.watch(function(error, result) {
          if (!error) {
            var timestamp = (new Date()).toUTCString()
            let cost = _this.state.web3.fromWei(result.args._newCostPerKb, "ether").toNumber()
            const newEvent = {value: timestamp + ": new CostChange Event, cost set to " + cost + " eth"}
            _this.setState({events: _this.state.events.concat(newEvent)})
            _this.setState({costPerKb: cost})
            _this.updateEthBalance()
          } else {
            console.error(error)
          }
        })

        insufficientFundsEvent.watch(function(error, result) {
          if (!error) {
            let value = _this.state.web3.fromWei(result.args._deficit, "ether").toNumber()
            var timestamp = (new Date()).toUTCString()
            const newEvent = {value: timestamp + ": new InsufficientFunds Event, deficit: " + value + " eth"}
            _this.setState({events: _this.state.events.concat(newEvent)})
          } else {
            console.error(error)
          }
        })

      })
    })
  }

  updateEthBalance() {
    var balance = this.state.web3.fromWei(this.state.web3.eth.getBalance(this.state.netOp), "ether")
    this.setState({netOpBalance: balance.toNumber()})
    balance = this.state.web3.fromWei(this.state.web3.eth.getBalance(this.state.scOwner), "ether")
    this.setState({scOwnerBalance: balance.toNumber()})
    balance = this.state.web3.fromWei(this.state.web3.eth.getBalance(this.state.contractInstance.address), "ether")
    this.setState({contractBalance: balance.toNumber()})
  }

  updateCredit() {
    this.state.contractInstance.getBalance({from:this.state.scOwner}).then((result) => {
      this.setState({credit: this.state.web3.fromWei(result, "ether").toNumber()})
    })
  }

  updateCost() {
    this.state.contractInstance.costPerKb.call({from:this.state.scOwner}).then((result) => {
      this.setState({costPerKb: this.state.web3.fromWei(result, "ether").toNumber()})
    })
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
            <a href="#" className="pure-menu-heading pure-menu-link">Truffle Box</a>
        </nav>

        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-1">
              <h1>SCaaS Demo</h1>
              <Balance name="the network operator" balance={this.state.netOpBalance} address={this.state.netOp} />
              <Provider ecgi={this.state.ecgi} costPerKb={this.state.costPerKb} balance={this.state.scOwnerBalance}
                        name="the small cell owner" address={this.state.scOwner} credit={this.state.credit}
                        contractBalance={this.state.contractBalance} />
            </div>
            <div className="pure-u-1-1">
              <UpdateBalanceButton instance={this.state.contractInstance} netOp={this.state.netOp} />
              <ClearBalanceButton instance={this.state.contractInstance} netOp={this.state.netOp} />
              <WithdrawButton instance={this.state.contractInstance} scOwner={this.state.scOwner} />
              <FundButton instance={this.state.contractInstance} netOp={this.state.netOp} web3={this.state.web3} />
            </div>
            <div className="pure-u-1-1">
              <PriceForm instance={this.state.contractInstance} netOp={this.state.netOp} web3={this.state.web3} />
            </div>
          </div>
          <div className="pure-g">
            <div className="pure-u-1-1" id="eventHistory">
              <h3>Event History</h3>
              <EventList events={this.state.events} />
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App
