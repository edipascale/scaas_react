pragma solidity ^0.4.4;

contract SCaaS {

  event ProviderRegistered(uint indexed _ecgi, address _provider, address _contract);
  event ContractTerminated(uint indexed _ecgi);
  address public owner;
  mapping (uint => address) public register;

  function SCaaS() public {
    owner = msg.sender;
  }

  /* Register a small cell provider (with his cell ID and its ethereum
   * address) with the address of the new contract instance
   */
  function registerProvider(uint _ecgi, address _provider, uint _costPerKb) public {
    require(msg.sender == owner);
    require(register[_ecgi] == 0);
    SCaaSInstance instance = new SCaaSInstance(owner, _ecgi, _provider, _costPerKb);
    register[_ecgi] = instance;
    ProviderRegistered(_ecgi, _provider, instance);
  }

  /* Terminate an existing contract with a small cell provider. We do it here
   * to make sure that the register is up to date, but in an implementation
   * using a normal DB at the provider we could skip this step and terminate
   * it directly at the instance.
   */
  function deregisterProvider(uint _ecgi) public returns(bool) {
    require(msg.sender == owner);
    require(register[_ecgi] != 0);
    SCaaSInstance instance = SCaaSInstance(register[_ecgi]);
    instance.terminate();
    register[_ecgi] = 0;
    ContractTerminated(_ecgi);
  }
}

contract SCaaSInstance{
  uint balance;
  address public owner;
  uint public ecgi;
  address public provider;
  uint public costPerKb;
  event BalanceUpdate(uint _valueAdded);
  event Withdrawal(uint _amount);
  event InsufficientFunds(uint deficit);
  event CostChange(uint _newCostPerKb);
  event FundIncrease(uint _value);
  event DebugBalanceCleared();

  modifier ownerOnly {
    require(msg.sender == owner);
    _;
  }

  function SCaaSInstance(address _owner, uint _ecgi, address _provider, uint _costPerKb) public {
    owner = _owner;
    ecgi = _ecgi;
    provider = _provider;
    costPerKb = _costPerKb;
  }

  /* Top up the balance of the contract so that providers can withdraw funds
   * to receive payment for the traffic they served.
   */
  function () public payable {
    if (msg.value > 0)
      FundIncrease(msg.value);
  }

  /* commented out as changing the owner of the instance but not of the landing
   * contract would cause deregisterProvider() calls to fail.
   *
   *  function changeOwner(address _newOwner) public ownerOnly {
   *    owner = _newOwner;
   *  }
   */

  /* Changes the cost per Kb in Wei for future calls to updateBalance().
   */
  function changeCost(uint _newCost) public ownerOnly {
    costPerKb = _newCost;
    CostChange(_newCost);
  }

  /* Only the conctract owner can call this, wallet file pre-laoded in android app
   */
  function updateBalance(uint _trafficInKb) public ownerOnly {
    uint toPay = _trafficInKb * costPerKb;
    assert(balance + toPay >= balance);
    balance += toPay;
    BalanceUpdate(toPay);
  }

  /* For debugging/testing purpose only - REMOVE IN PRODUCTION
   * Clears the outstanding balance of the provider, so that we can "reset" the test.
   */
  function clearBalance() public ownerOnly {
    if (balance > 0) {
      balance = 0;
      DebugBalanceCleared();
    }
  }

  /* Convenience function to get the current balance. can only be invoked by the provider.
   */
  function getBalance() constant public returns(uint) {
    require(msg.sender == provider);
    return balance;
  }

  /* Allows providers to withdraw any outstanding credit after the firing of
   * a BalanceUpdate event
   */
  function withdraw() public returns (bool) {
    require(msg.sender == provider);
    if (balance <= 0)
      return false;
    uint amount = balance;
    if (this.balance >= amount) {
      balance = 0;
      if (msg.sender.send(amount)) {
        Withdrawal(amount);
        return true;
      } else {
        balance = amount;
      }
    }
    InsufficientFunds(amount - this.balance);
    return false;
  }

  /* Maybe we should check for any outstanding balance and try to pay it before
   * we allow the termination to take place. Then again what happens if there
   * are not enough funds? A provider might prevent an operator to ever terminate
   * the contract by never calling Withdraw(). Tricky one.
   */
  function terminate() public ownerOnly {
    selfdestruct(owner);
  }
}
