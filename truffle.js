module.exports = {
  migrations_directory: "./migrations",
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*", // Match any network id
      gas: 4612388 // Gas limit used for deploys
    },
    rinkeby: {
      host: "localhost",
      port: 8545,
      network_id: 4,
      from: "0xfadb1869970b41903976b53bfbec49a88dac6424",
      gas: 4612388 // Gas limit used for deploys
    }
  }
};
